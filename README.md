# AMD-PState-Guide

Guide on how to use /setup AMD Pstate scaling driver
----------------------------------------------------


**Original Pstate Driver**
----------------------------------------------------

It should be noted this is only for Zen2 and newer


First check if support is already enabled with
    `lscpu | grep cppc`

Make sure CPPC is enabled in your BIOS/UEFI, some its on by default others its not. Exactly how is Bios dependant

for the original Pstate driver you will need to add this to your kernel command line
    `amd_pstate=passive`

if Pstate driver doesnt load automatically you can do this to load it on boot by creating a file (this shouldnt be necessary but you can force it otherwise skip to confirming)
    `/etc/modules-load.d/amd-pstate.conf`

in that file it should say

    # Load amd pstate at boot
    amd_pstate

confirming its working you can run
    `cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_driver`

and it will say
    `amd-pstate`

----------------------------------------------------



**AMD Pstate EPP Driver (needs kernel 6.3+)** 
----------------------------------------------------

to use the EPP driver you will need to add this to your kernel commandline
    `amd_pstate=active`


confirming its working you can run
    `cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_driver`

and it will say
    `amd_pstate_epp`

to check current performance hint use
    `cat /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference`

to see available performance hints run
    `cat /sys/devices/system/cpu/cpu0/cpufreq/energy_performance_available_preferences`

to set the performance hint run (replace balance_performance with desired hint)
    `echo "balance_performance" | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference balance_performance`

you can also install the latest power-profiles-daemon to set the performance hint within the Gnome/KDE power settings
